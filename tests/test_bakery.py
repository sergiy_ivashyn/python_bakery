import unittest
# append parent directory to import path
import env
# now we can import the module to be tested
from bakery.bakery import * 

"""
Test cases for bakery.

Some comments are taken from:
 https://docs.python.org/2/library/unittest.html
 http://pythontesting.net/framework/unittest/unittest-introduction/

"""

class BreadBakingTestCase(unittest.TestCase):
    def runTest(self):
        """
        Bread baking test case.

        The simplest TestCase subclass will simply override the runTest() 
        method in order to perform specific testing code.

        NB: this method will be executed in each sub-testcase.
        """
        pass

class DefaultBreadBakingTestCase(BreadBakingTestCase):
    def test_genericBreadBaking(self):
        """
        We need to bake bread
        """
        self.bakeHouse = BakeHouse()
        self.bread = self.bakeHouse.BakeBread()


"""
This allows us to run all of the test code just by running the file.
Running it with no options is the most terse, 
and running with a '-v' is more verbose, showing which tests ran.

"""
if __name__ == '__main__':
    unittest.main()

