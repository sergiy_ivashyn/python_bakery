"""
Inspired by: http://stackoverflow.com/questions/61151/where-do-the-python-unit-tests-go

If we put the tests in tests folder, test can't import ..lib in CLI 
because __main__ can't import relative module.
We can use nose, or we can add parent directory to python import path, 
for that we create this file.

module/
  lib/
    __init__.py
    module.py
    etc
  tests/
    env.py
    test_module.py
    test_module2.py
    etc

"""

import sys
import os

# append module root directory to sys.path
sys.path.append(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__)
        )
    )
)

