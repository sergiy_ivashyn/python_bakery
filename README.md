# *Python_bakery*

## Overview
This is a Python study projects aimed mainly 
at illustrating the use and implementation of
a few simple design patterns in Python.

More than that, one can consider this an example
of avoiding the misuse or over-use of the patterns.

The whole story is very much inspired by the discussion
and an article in habrahabr and in the blog of stdray.


## References

[habrahabr] (http://habrahabr.ru/post/153225/)

[stdray] (http://stdray.livejournal.com/74041.html)

## Credits

So far the code was/is written by s.ivashyn_at_gmail.com


